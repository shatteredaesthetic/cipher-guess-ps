module Data.GameEnvironment where

newtype GameEnvironment = GameEnvironment
  { low :: Int, high :: Int, tolerance :: Int }

gameEnvironment :: Int -> Int -> Int -> GameEnvironment
gameEnvironment l h tol =
  GameEnvironment { low: l, high: h, tolerance: tol }
