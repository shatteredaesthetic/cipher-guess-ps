module Game where

import Prelude
import Data.Array as A
import Control.Monad.RWS (RWS)
import Control.Monad.Reader.Class (ask)
import Control.Monad.State.Class (get)
import Control.Monad.Writer.Class (tell)
import Data.GameEnvironment (GameEnvironment(..))
import Data.GameState (GameState(..))
import Data.Maybe (Maybe(..))

type Log = Array String

type Game = RWS GameEnvironment Log GameState

logic :: Int -> { low :: Int, high :: Int, tolerance :: Int } -> Int -> Game (Maybe Int)
logic g { low: l, high: h, tolerance: tol } t
  | g < l = do
    tell $ A.singleton "Too low for bounds."
    pure Nothing
  | g > h = do
    tell $ A.singleton "Too high for bounds."
    pure Nothing
  | t == g = do
    tell $ A.singleton "That's it, Exactly!"
    pure $ Just t
  | g < t - tol = do
    tell $ A.singleton "That's too low, try again."
    pure Nothing
  | g > t + tol = do
    tell $ A.singleton "That's too high, try again."
    pure Nothing
  | otherwise = do
    tell $ A.singleton $ "Close Enough! The number was " <> show t
    pure $ Just t

game :: Int -> Game (Maybe Int)
game guess = do
  GameEnvironment cfg <- ask
  GameState target <- get
  logic guess cfg target
