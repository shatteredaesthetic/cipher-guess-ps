module Main where

import Prelude
import Control.Monad.RWS (evalRWS)
import Data.Foldable (for_)
import Data.GameState (GameState(..))
import Data.GameEnvironment (GameEnvironment, gameEnvironment)
import Data.Int (fromStringAs, decimal)
import Data.Maybe (Maybe(..))
import Data.Tuple (Tuple(..))
import Effect (Effect)
import Effect.Console (log)
import Effect.Random (randomInt)
import Game (game)
import Node.ReadLine as RL

runGame :: GameEnvironment -> GameState -> Effect Unit
runGame env state = do
  interface <- RL.createConsoleInterface RL.noCompletion
  RL.setPrompt "> " 2 interface

  let
    lineHandler :: String -> Effect Unit
    lineHandler input =
      case fromStringAs decimal input of
        Just guess -> do
          let Tuple result written = evalRWS (game guess) env state
          for_ written log
          case result of
            Just _ -> do
              RL.close interface
            Nothing -> do
              log "Guess again:"
              RL.close interface
              runGame env state
        Nothing -> do
          log "Try again, but with a whole number this time."
          RL.close interface
          runGame env state

  RL.setLineHandler interface lineHandler
  log "Guess a whole number between 1 and 100:"
  RL.prompt interface

  pure unit

main :: Effect Unit
main = void do
  let env = gameEnvironment 1 100 3
  rnd <- randomInt 1 100
  runGame env $ GameState rnd
