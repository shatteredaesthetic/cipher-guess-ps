# cipher-guess-ps

This is the source code for a tutorial I am writing about using algebraic data types and functional programming in javascript. It is a number guessing game! Exciting!

**_NOTE:_** This is a Purescript rewrite of the javascript code base [here](https://github.com/shatteredaesthetic/cipher-guess).

### Install

You need to have Purescript installed, so go [here](http://www.purescript.org/) and download the right package for your system. then get the Purescript tools by running

```
$ npm install -g pulp bower
```

Clone this repo onto your local system, navigate to the folder and run:

```
$ pulp build
```

This will install the dependencies for this project. At this point you're good to go!

#### Run some code

To get started running the code, and installing dependencies, just run the following in the project root:

```
$ pulp run
```

#### Test some code

Testing is set up with `tape`.

Specs live in the `test` directory and can be run in one of two ways, each with it's own command.

**_NOTE:_** Tests will be added in a future post, so, for now, it's empty. I know, I know...

To run all of your specs and exit at the end with the results, all you need to do is run:

```
$ pulp test
```
